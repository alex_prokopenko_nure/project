#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <cmath>
#include <ctime>
#include <string>
#include <limits>

std::vector<std::vector<std::vector<long long>>> profiles;
long long answer;
int columns, rows;

int bit(int num, int pos) {
	num &= 1 << pos;
	if (num)
		return 1;
	return num;
}

void initiate(int columns, int rows) {
	std::cout << "Test with " << columns << " columns and " << rows << " rows:" << std::endl;
	profiles.clear();
	profiles.resize(columns);
	for (int i = 0; i < columns; ++i) {
		profiles[i].resize(rows);
		for (int j = 0; j < rows; ++j) {
			profiles[i][j].resize(1 << rows);
		}
	}
	profiles[0][0][0] = 1;
	answer = 0;
}
void trowelling(bool memory, bool time);

bool R(std::string s) { return (std::count_if(s.begin(), s.end(), isdigit) == s.size() && s.size() < 9) ? true : false; }

void input() {
	std::string first, second;
	std::cout << "Enter number of columns and number of rows" << std::endl;
	std::cin >> first >> second;
	if (R(first) && R(second)) {
		columns = std::stoi(first);
		rows = std::stoi(second);
	}
	else {
		std::cout << "Non-number input" << std::endl;
		return;
	}
	if (columns < rows)
		std::swap(columns, rows);
	if ((columns > 0 && rows > 0) && ((columns <= 19 && rows <= 19) || (columns <= 40 && rows <= 16))) {
		if (columns >= 16 && rows >= 16) {
			initiate(columns, rows);
			trowelling(true, true);
		}
		else {
			initiate(columns, rows);
			trowelling(true, true);
		}
	}
	else {
		std::cout << "Incorrect input:\n";
		if(columns <= 0 || rows <= 0)
			std::cout << "Values should be bigger than zero" << std::endl;
		if(columns > 18 && rows > 18)
			std::cout << "If one value is bigger than 19, another should be less than 17" << std::endl;
	}
}

void file_input(std::string file) {
	std::ifstream fin;
	fin.open("Tests\\" + file);
	if (!fin) {
		std::cout << "Folder with tests has not found" << std::endl;
		return;
	}
	int n;
	fin >> n;
	for (int i = 0; i < n; ++i) {
		std::string first, second;
		fin >> first >> second;
		if (R(first) && R(second)) {
			columns = std::stoi(first);
			rows = std::stoi(second);
		}
		else {
			std::cout << "Non-number input" << std::endl;
			continue;
		}
		if (columns < rows)
			std::swap(columns, rows);
		if ((columns > 0 && rows > 0) && ((columns <= 19 && rows <= 19) || (columns <= 40 && rows <= 16))) {			
			initiate(columns, rows);
			if (file == "normal_test.txt")
				trowelling(false, false);
			if (file == "broken_test.txt")
				trowelling(false, false);
			if (file == "limit_test.txt")
				trowelling(true, true);
			if (file == "memory_test.txt")
				trowelling(true, false);
			if (file == "time_test.txt")
				trowelling(false, true);
			if (file == "big_test.txt")
				trowelling(true, true);
		}
		else {
			std::cout << "Incorrect input:\n";
			if (columns <= 0 || rows <= 0)
				std::cout << "Values should be bigger than zero" << std::endl;
			if (columns > 19 && rows > 19)
				std::cout << "If one value is bigger than 12, another should be not bigger than 18" << std::endl;
			if (columns > 40 || rows > 16)
				std::cout << "If one value is between 18 and 40, another should be less than 17" << std::endl;
		}
	}
}

void trowelling(bool memory, bool time) {
	if(memory)
		std::cout << "Memory spent: " << (double)(sizeof(std::vector<std::vector<std::vector<long long>>>) + sizeof(std::vector<std::vector<long long>>) * columns + sizeof(std::vector<long long>) * rows * columns + sizeof(long long) * rows * columns * (1 << rows)) / 1000 << " KB" << std::endl;
	bool wrong = false;
	unsigned int start = clock();
	for (int col = 0; col < columns; ++col) {
		for (int row = 0; row < rows; ++row) {
			for (int mask = 0; mask < (1 << rows); ++mask) {
				if (profiles[col][row][mask] < 0) {
					wrong = true;
				}
					if (profiles[col][row][mask]) {
						if (bit(mask, row)) {
							if (row < rows - 1)
								profiles[col][row + 1][mask - (1 << row)] +=
								profiles[col][row][mask];
							else if (col < columns - 1)
								profiles[col + 1][0][mask - (1 << row)] += profiles[col][row][mask];
							else if (col == columns - 1 && row == rows - 1)
								answer += profiles[col][row][mask];
						}
						else {
							if (!bit(mask, row + 1)) {
								if (row == rows - 2) {
									if (col < columns - 1)
										profiles[col + 1][0][mask] += profiles[col][row][mask];
									else if (col == columns - 1 && row == rows - 2 )
										answer += profiles[col][row][mask];
								}
								else if (row != rows - 1) {
									profiles[col][row + 2][mask] += profiles[col][row][mask];
								}
							}
							if (col < columns - 1) {
								if (row < rows - 1) {
									profiles[col][row + 1][mask + (1 << row)] +=
										profiles[col][row][mask];
								}
								else {
									profiles[col + 1][0][mask + (1 << row)] +=
										profiles[col][row][mask];
								}
							}
						}
					}
				
			}
		}
	}
	unsigned int finish = clock();
	if(time)
		std::cout << "Time spent: " << finish - start << " ms" << std::endl;
	if (wrong || answer < 0)
		std::cout << "Long long overflow! Result is incorrect" << std::endl;
	std::cout << "Result: " << answer << std::endl;
	std::cout << std::endl;
}

int main() { 
	std::string kuku;
	for (;;) {
		std::cout << "\nEnter '1' to test a program with default tests\nEnter '2' to test program with your own tests\nEnter anything else to exit program" << std::endl;
		std::cin >> kuku;
		if (kuku[0] == '1') {
			for (;;) {
				std::cout << "\nEnter '1' to start normal tests\nEnter '2' to start broken tests\nEnter '3' to start limit tests\nEnter '4' to start memory tests\nEnter '5' to start time tests\nEnter '6' to start big tests\nEnter anything else to exit" << std::endl;
				std::string kind;
				std::cin >> kind;
				if (kind[0] == '1')
					file_input("normal_test.txt");
				else if (kind[0] == '2')
					file_input("broken_test.txt");
				else if (kind[0] == '3')
					file_input("limit_test.txt");
				else if (kind[0] == '4')
					file_input("memory_test.txt");
				else if (kind[0] == '5')
					file_input("time_test.txt");
				else if (kind[0] == '6')
					file_input("big_test.txt");
				else
					break;
			}
			continue;
		}
		else if (kuku[0] == '2') {
			for (;;) {
				std::cout << "\nEnter '1' to enter your values\nEnter anything else to exit" << std::endl;
				std::string in;
				std::cin >> in;
				if (in[0] == '1') {
					input();
					continue;
				}
				else {
					break;
				}
				break;
			}
			continue;
		}
		else {
			break;
		}
		break;
	}

	return 0;
}
