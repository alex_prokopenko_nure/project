﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace WindowsFormsApplication2
{
    class Desk
    {
        public int X { get; set; }
        public int Y { get; set; }
        public Square[,] Matrix;
        public Desk(int x, int y)
        {
            X = x; Y = y;
            Matrix = new Square[x, y];
            for(int i = 0; i < x; ++i)
            {
                for(int j = 0; j < y; ++j)
                {
                    Matrix[i, j] = new Square(40 * (i) + 5, 40 * (j) + 5, 40);
                }
            }
        }
        public void Draw(Graphics g)
        {
            for (int i = 0; i < X; ++i)
            {
                for (int j = 0; j < Y; ++j)
                {
                    Matrix[i, j].Draw(g);
                }
            }
        }
        public void DrawProfile(int x, int y, int mask, Graphics g, int x1, int y1, int x2, int y2)
        {
            for(int i = 0; i < x; ++i)
            {
                for(int j = 0; j < Y; ++j)
                {
                    Matrix[i, j].Fill(g, Color.Gray);
                }
            }
            for(int i = Y - 1; i >= 0; --i)
            {
                if(i >= y)
                {
                    int n = mask & (1 << i);
                    if(n != 0)
                    {
                        Matrix[x, i].Fill(g, Color.DarkRed);
                    }
                    else
                    {
                        Matrix[x, i].Fill(g, Color.Red);
                    }
                }
                else
                {
                    Matrix[x, i].Fill(g, Color.Gray);
                }                    
            }
            for(int i = y; i >= 0; --i)
            {
                if(x + 1 < X)
                {
                    int n = mask & (1 << i);
                    if (n != 0)
                    {
                        Matrix[x + 1, i].Fill(g, Color.DarkRed);
                    }
                    else
                    {
                        Matrix[x + 1, i].Fill(g, Color.Red);
                    }
                    if(i == y)
                    {
                        Matrix[x + 1, i].Fill(g, Color.Red);
                    }
                }
            }
            if (x1 != -1)
            {
                Matrix[x1, y1].Fill(g, Color.DarkGreen);
                Matrix[x2, y2].Fill(g, Color.DarkGreen);
            }
        }
        public void DrawAlgo(int x, int y, int mask, Graphics g)
        {
            for (int i = 0; i < x; ++i)
            {
                for (int j = 0; j < Y; ++j)
                {
                    Matrix[i, j].Fill(g, Color.Gray);
                }
            }
            for (int i = Y - 1; i >= 0; --i)
            {
                if (i >= y)
                {
                    int n = mask & (1 << i);
                    if (n != 0)
                    {
                        Matrix[x, i].Fill(g, Color.DarkRed);
                    }
                    else
                    {
                        Matrix[x, i].Fill(g, Color.Red);
                    }
                }
                else
                {
                    Matrix[x, i].Fill(g, Color.Gray);
                }
            }
            for (int i = Y - 1; i >= 0; --i)
            {
                if (x + 1 < X && i < y)
                {
                    int n = mask & (1 << i);
                    if (n != 0)
                    {
                        Matrix[x + 1, i].Fill(g, Color.DarkRed);
                    }
                    else
                    {
                        Matrix[x + 1, i].Fill(g, Color.Red);
                    }
                }
                else if (x + 1 < X && i == y)
                {
                    Matrix[x + 1, i].Fill(g, Color.Red);
                }
                else if (x + 1 < X && i > y)
                {
                    Matrix[x + 1, i].Fill(g, Color.White);
                }
            }
            for (int i = x + 2; i < X; ++i)
            {
                for (int j = 0; j < Y; ++j)
                {
                    Matrix[i, j].Fill(g, Color.White);
                }
            }
        }
        
    }
}
