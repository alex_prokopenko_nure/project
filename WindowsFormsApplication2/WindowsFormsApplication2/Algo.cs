﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;

namespace WindowsFormsApplication2
{
    public partial class Algo : Form
    {
        Matrix m = new Matrix(Coords.X, Coords.Y);
        Desk d = new Desk(Coords.X, Coords.Y);
        public Algo()
        {
            InitializeComponent();
        }
        
        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            if (textBox5.Text != "" && textBox4.Text != "")
            {
                try
                {
                    if (Convert.ToInt32(textBox5.Text) >= 0 && Convert.ToInt32(textBox5.Text) < (1 << m.rows) && Convert.ToInt32(textBox4.Text) >= 0 && Convert.ToInt32(textBox4.Text) < m.rows)
                    {
                        m.DrawManual(g, Convert.ToInt32(label7.Text), Convert.ToInt32(textBox5.Text), Convert.ToInt32(textBox4.Text));
                    }
                    else
                    {
                        m.DrawManual(g, Convert.ToInt32(label7.Text), 0, 0);
                    }
                }
                catch
                {
                    m.DrawManual(g, Convert.ToInt32(label7.Text), 0, 0);
                }
            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(label7.Text) + 1 != m.cols)
            {
                label7.Text = Convert.ToString(Convert.ToInt32(label7.Text) + 1);
                PaintEventArgs pe = new PaintEventArgs(panel1.CreateGraphics(), new Rectangle(panel1.Location.X, panel1.Location.Y, panel1.Width, panel1.Height));
                Graphics g = pe.Graphics;
                SolidBrush sb = new SolidBrush(Color.White);
                g.FillRectangle(sb, 0, 0, panel1.Width, panel1.Height);
                panel1_Paint(panel1, pe);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(label7.Text) != 0)
            {
                label7.Text = Convert.ToString(Convert.ToInt32(label7.Text) - 1);
                PaintEventArgs pe = new PaintEventArgs(panel1.CreateGraphics(), new Rectangle(panel1.Location.X, panel1.Location.Y, panel1.Width, panel1.Height));
                Graphics g = pe.Graphics;
                SolidBrush sb = new SolidBrush(Color.White);
                g.FillRectangle(sb, 0, 0, panel1.Width, panel1.Height);
                panel1_Paint(panel1, pe);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (!(m.X == m.cols))
            {
                m.NextStep();
                label2.Text = Convert.ToString(m.X);
                label3.Text = Convert.ToString(m.Y);
                label5.Text = Convert.ToString(m.Mask);
                label9.Text = Convert.ToString(m.Answer);
                label11.Text = "Transition to: ";
                for (int i = 0; i < m.Info.Last().Count; ++i)
                {
                    label11.Text += m.Info.Last()[i] + ", ";
                }
                PaintEventArgs pe = new PaintEventArgs(panel1.CreateGraphics(), new Rectangle(panel1.Location.X, panel1.Location.Y, panel1.Width, panel1.Height));
                PaintEventArgs pe2 = new PaintEventArgs(panel2.CreateGraphics(), new Rectangle(panel2.Location.X, panel2.Location.Y, panel2.Width, panel2.Height));
                Graphics g = pe.Graphics;
                SolidBrush sb = new SolidBrush(Color.White);
                g.FillRectangle(sb, 0, 0, panel1.Width, panel1.Height);
                try
                {
                    panel1_Paint(panel1, pe);
                    panel2_Paint(panel2, pe2);
                }
                catch 
                {
                    MessageBox.Show("That's all folks!");
                }
            }
            else
            {
                MessageBox.Show("Make a stepback or reinitiate the field");
            }
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {
            try
            {
                Graphics g = e.Graphics;
                d.Draw(g);
                d.DrawAlgo(m.X, m.Y, m.Mask, g);
            }
            catch
            {
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (!(m.X == 0 && m.Y == 0 && m.Mask == 0))
            {
                m.BackStep();
                label2.Text = Convert.ToString(m.X);
                label3.Text = Convert.ToString(m.Y);
                label5.Text = Convert.ToString(m.Mask);
                label9.Text = Convert.ToString(m.Answer);
                label11.Text = "Transition to: ";
                if (m.Info.Count != 0)
                {
                    for (int i = 0; i < m.Info.Last().Count; ++i)
                    {
                        label11.Text += m.Info.Last()[i] + ", ";
                    }
                }
                PaintEventArgs pe = new PaintEventArgs(panel1.CreateGraphics(), new Rectangle(panel1.Location.X, panel1.Location.Y, panel1.Width, panel1.Height));
                PaintEventArgs pe2 = new PaintEventArgs(panel2.CreateGraphics(), new Rectangle(panel2.Location.X, panel2.Location.Y, panel2.Width, panel2.Height));
                Graphics g = pe.Graphics;
                SolidBrush sb = new SolidBrush(Color.White);
                g.FillRectangle(sb, 0, 0, panel1.Width, panel1.Height);
                try
                {
                    panel1_Paint(panel1, pe);
                    panel2_Paint(panel2, pe2);
                }
                catch 
                {
                    MessageBox.Show("That's all folks!");
                }
            }
            else
            {
                MessageBox.Show("Nowhere to step back");
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(textBox1.Text) >= 0)
                {
                    for (;;)
                    {
                        if (m.X != m.cols)
                        {
                            System.Threading.Thread.Sleep(Convert.ToInt32(textBox1.Text));
                            button3_Click(sender, e);
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Enter delay bigger or equal to zero");
                }
            }
            catch
            {
                MessageBox.Show("Enter number, not a string");
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            try
            {
                if (textBox3.Text != "" && textBox2.Text != "" && Convert.ToInt32(textBox3.Text) > 0 && Convert.ToInt32(textBox2.Text) > 0 && Convert.ToInt32(textBox3.Text) < 21 && Convert.ToInt32(textBox2.Text) < 11)
                {
                    label7.Text = "0";
                    m = new Matrix(Convert.ToInt32(textBox3.Text), Convert.ToInt32(textBox2.Text));
                    d = new Desk(Convert.ToInt32(textBox3.Text), Convert.ToInt32(textBox2.Text));
                    PaintEventArgs pe = new PaintEventArgs(panel1.CreateGraphics(), new Rectangle(panel1.Location.X, panel1.Location.Y, panel1.Width, panel1.Height));
                    PaintEventArgs pe2 = new PaintEventArgs(panel2.CreateGraphics(), new Rectangle(panel2.Location.X, panel2.Location.Y, panel2.Width, panel2.Height));
                    Graphics g = pe.Graphics;
                    Graphics g2 = pe2.Graphics;
                    SolidBrush sb = new SolidBrush(Color.White);
                    g.FillRectangle(sb, 0, 0, panel1.Width, panel1.Height);
                    g2.FillRectangle(sb, 0, 0, panel2.Width, panel2.Height);
                    panel1_Paint(panel1, pe);
                    panel2_Paint(panel2, pe2);
                }
                else
                {
                    MessageBox.Show("Oops, wrong input!");
                }
            }
            catch
            {
                MessageBox.Show("Enter number, not a string");
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(textBox1.Text) >= 0)
            {
                for (;;)
                {
                    if ((m.X >= 0 && m.Y >= 0) && !(m.X == 0 && m.Y == 0 && m.Mask == 0))
                    {
                        m.BackStep();
                        label2.Text = Convert.ToString(m.X);
                        label3.Text = Convert.ToString(m.Y);
                        label5.Text = Convert.ToString(m.Mask);
                        label9.Text = Convert.ToString(m.Answer);
                        label11.Text = "Transition to: ";
                        if (m.Info.Count != 0)
                        {
                            for (int i = 0; i < m.Info.Last().Count; ++i)
                            {
                                label11.Text += m.Info.Last()[i] + ", ";
                            }
                        }
                        PaintEventArgs pe = new PaintEventArgs(panel1.CreateGraphics(), new Rectangle(panel1.Location.X, panel1.Location.Y, panel1.Width, panel1.Height));
                        PaintEventArgs pe2 = new PaintEventArgs(panel2.CreateGraphics(), new Rectangle(panel2.Location.X, panel2.Location.Y, panel2.Width, panel2.Height));
                        Graphics g = pe.Graphics;
                        SolidBrush sb = new SolidBrush(Color.White);
                        g.FillRectangle(sb, 0, 0, panel1.Width, panel1.Height);
                        try
                        {
                            panel1_Paint(panel1, pe);
                            panel2_Paint(panel2, pe2);
                        }
                        catch 
                        {
                            MessageBox.Show("That's all folks!");
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            }
            else
            {
                MessageBox.Show("Enter delay bigger or equal to zero");
            }
            }
            catch
            {
                MessageBox.Show("Enter number, not a string");
            }
        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void button8_Click(object sender, EventArgs e)
        {
            try
            {
                if (textBox5.Text != "" && textBox4.Text != "")
                    if (Convert.ToInt32(textBox5.Text) >= 0 && Convert.ToInt32(textBox5.Text) < (1 << m.rows) && Convert.ToInt32(textBox4.Text) >= 0 && Convert.ToInt32(textBox4.Text) < m.rows)
                    {
                        PaintEventArgs pe = new PaintEventArgs(panel1.CreateGraphics(), new Rectangle(panel1.Location.X, panel1.Location.Y, panel1.Width, panel1.Height));
                        Graphics g = pe.Graphics;
                        SolidBrush sb = new SolidBrush(Color.White);
                        g.FillRectangle(sb, 0, 0, panel1.Width, panel1.Height);
                        panel1_Paint(panel1, pe);
                    }
            }
            catch
            {
                MessageBox.Show("Enter number, not a string");
            }
        }

        private void pictureBox5_MouseHover(object sender, EventArgs e)
        {
            Cursor = Cursors.Hand;
        }

        private void pictureBox5_MouseLeave(object sender, EventArgs e)
        {
            Cursor = Cursors.Default;
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            MessageBox.Show("This button allows you to make one step of algorithm");
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("This button allows you to make backstep");
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("By entering values in fields above, you can specify from which values massive will be shown");
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Enter values in fields above to initiate new field");
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Enter delay and click button 'auto' to make steps automaticaly with entered delay\n'reverse' to make backsteps with entered delay");
        }
    }
}
