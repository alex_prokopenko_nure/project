﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;


namespace WindowsFormsApplication2
{
    class Matrix
    {
        public int[, ,] profiles { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int Mask { get; set; }
        public int cols { get; set; }
        public int rows { get; set; }
        public int Answer { get; set; }
        public List<string> Log { get; set; }
        public List<List<string>> Info { get; set; }
        public Matrix(int x, int y)
        {
            cols = x;
            rows = y;
            Answer = 0;
            profiles = new int[x, y, (int)Math.Pow(2, y)];
            profiles[0, 0, 0] = 1;
            X = 0;
            Y = 0;
            Mask = 0;
            Log = new List<string>();
            Info = new List<List<string>>();
        }
        private int bit(int num, int pos)
        {
            num &= 1 << pos;
            if (num != 0)
                return 1;
            return num;
        }
        public void BackStep()
        {
            string last = Log.Last();
            List<string> trans = Info.Last();
            string[] tmp = last.Split(' ');
            X = Convert.ToInt32(tmp[0]); Y = Convert.ToInt32(tmp[1]); Mask = Convert.ToInt32(tmp[2]);
            int x, y, mask;
            for (int i = 0; i < trans.Count; ++i)
            {
                string[] temp = trans[i].Split(' ');
                x = Convert.ToInt32(temp[0]); y = Convert.ToInt32(temp[1]); mask = Convert.ToInt32(temp[2]);
                if (x == 999)
                    Answer -= profiles[X, Y, Mask];
                else
                    profiles[x, y, mask] -= profiles[X, Y, Mask];
            }
            Log.Remove(Log.Last());
            Info.Remove(Info.Last());
        }
        public void NextStep()
        {
            bool indicator = false;
            bool iter = false;
            Log.Add(X + " " + Y + " " + Mask);
            Info.Add(new List<string>());
            for (int col = 0; col < cols; ++col)
            {
                for (int row = 0; row < rows; ++row)
                {
                    for (int mask = 0; mask < (1 << rows); ++mask)
                    {
                        if (!iter)
                        {
                            col = X;
                            row = Y;
                            mask = Mask;
                        }
                        if (profiles[col, row, mask] != 0)
                        {
                            if (indicator)
                            {
                                X = col;
                                Y = row;
                                Mask = mask;
                                col = cols;
                                row = rows;
                                break;
                            }
                                if (bit(mask, row) != 0)
                                {
                                if (row < rows - 1)
                                {
                                    profiles[col, row + 1, mask - (1 << row)] +=
                                  profiles[col, row, mask];
                                    Info.Last().Add(col + " " + (row + 1) + " " + (mask - (1 << row)));
                                }
                                else if (col < cols - 1)
                                {
                                    profiles[col + 1, 0, mask - (1 << row)] += profiles[col, row, mask];
                                    Info.Last().Add((col + 1) + " " + 0 + " " + (mask - (1 << row)));
                                }
                                else if (col == cols - 1 && row == rows - 1) {
                                    Answer += profiles[col, row, mask];
                                    Info.Last().Add(999 + " " + 999 + " " + 999);
                                }
                                }
                                else
                                {
                                    if (bit(mask, row + 1) == 0)
                                    {
                                        if (row == rows - 2)
                                        {
                                            if (col < cols - 1) { 
                                                profiles[col + 1, 0, mask] += profiles[col, row, mask];
                                                Info.Last().Add((col + 1) + " " + 0 + " " + mask);
                                            }
                                            else if (col == cols - 1 && row == rows - 2 ) { 
                                                Answer += profiles[col, row, mask];
                                                Info.Last().Add(999 + " " + 999 + " " + 999);
                                            }
                                        }
                                        else if (row != rows - 1)
                                        {
                                            profiles[col, row + 2, mask] += profiles[col, row, mask];
                                        Info.Last().Add(col + " " + (row + 2) + " " + mask);
                                    }
                                }
                                    if (col < cols - 1)
                                    {
                                        if (row < rows - 1)
                                        {
                                            profiles[col, row + 1, mask + (1 << row)] +=
                                            profiles[col, row, mask];
                                        Info.Last().Add(col + " " + (row + 1) + " " + (mask + (1 << row)));
                                        }
                                        else
                                        {
                                            profiles[col + 1, 0, mask + (1 << row)] +=
                                            profiles[col, row, mask];
                                        Info.Last().Add((col + 1) + " " + 0 + " " + (mask + (1 << row)));
                                    }
                                    }
                                }
                            indicator = true;
                            if (X + 1 == cols && Y + 1 == rows && indicator)
                            {
                                X = cols;
                                Y = 0;
                                Mask = 0;
                            }
                        }
                        iter = true;
                    }
                }
            }
            if(X == cols - 1 && Y == rows - 2 && Mask == 0 && Empty() && Answer != 0)
            {
                X = cols;
                Y = 0;
                Mask = 0;
            }
        }
        private bool Empty()
        {
            for(int i = rows - 2; i < rows; ++i)
            {
                for(int j = 0; j < (1 << rows); ++j)
                {
                    if (i == rows - 2 && j == 0)
                        continue;
                    if(profiles[cols - 1, i, j] != 0)
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        public void Draw(Graphics g, int c)
        {
            for(int i = 0; i < rows; ++i)
            {
                Font f = new Font("Arial", 14, FontStyle.Bold);
                g.DrawString(Convert.ToString(i), f, Brushes.Black, 0, 40 * (i + 1));
            }
            for (int j = 0; j < (1 << rows); ++j)
            {
                Font f = new Font("Arial", 14, FontStyle.Bold);
                g.DrawString(Convert.ToString(j), f, Brushes.Black, 40 * (j + 1), 0);
            }
            for (int i = 0; i < rows; ++i)
            {
                for(int j = 0; j < 1 << rows; ++j)
                {
                    Pen pen = new Pen(Color.Black);
                    pen.Width = 5;
                    Font f = new Font("Arial", 14, FontStyle.Bold);
                    g.DrawRectangle(pen, 40 * (j + 1), 40 * (i + 1), 40, 40);
                    if (profiles[c, i, j] != 0)
                        g.DrawString(Convert.ToString(profiles[c, i, j]), f, Brushes.Green, 40 * (j + 1) + 2, 40 * (i + 1) + 2);
                    else
                        g.DrawString(Convert.ToString(profiles[c, i, j]), f, Brushes.Black, 40 * (j + 1) + 2, 40 * (i + 1) + 2);
                }
            }
        }
        public void DrawManual(Graphics g, int c, int x, int y)
        {
            for (int i = y; i < Math.Min(y + 8, rows); ++i)
            {
                Font f = new Font("Arial", 14, FontStyle.Bold);
                g.DrawString(Convert.ToString(i), f, Brushes.Black, 0, 40 * ((i + 1) - y));
            }
            for (int j = x; j < Math.Min(x + 32, 1 << rows); ++j)
            {
                Font f = new Font("Arial", 14, FontStyle.Bold);
                g.DrawString(Convert.ToString(j), f, Brushes.Black, 40 * ((j + 1) - x), 0);
            }
            for (int i = y; i < Math.Min(y + 8, rows); ++i)
            {
                for (int j = x; j < Math.Min(x + 32, 1 << rows); ++j)
                {
                    Pen pen = new Pen(Color.Black);
                    pen.Width = 5;
                    Font f = new Font("Arial", 14, FontStyle.Bold);
                    g.DrawRectangle(pen, 40 * ((j + 1) -  x), 40 * ((i + 1) - y), 40, 40);
                    if (profiles[c, i, j] != 0)
                        g.DrawString(Convert.ToString(profiles[c, i, j]), f, Brushes.Green, 40 * ((j + 1) - x) + 2, 40 * ((i + 1) - y) + 2);
                    else
                        g.DrawString(Convert.ToString(profiles[c, i, j]), f, Brushes.Black, 40 * ((j + 1) - x) + 2, 40 * ((i + 1) - y) + 2);
                }
            }
        }
    }
}
