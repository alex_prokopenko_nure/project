﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class ProfileDP : Form
    {
        Desk desk = new Desk(Coords.X, Coords.Y);
        Stack<char> stack = new Stack<char>();
        Stack<int> stackx = new Stack<int>();
        Stack<int> stacky = new Stack<int>();
        Stack<int> stackm = new Stack<int>();
        int x = 0, y = 0, mask = 0;

        private void button2_Click(object sender, EventArgs e)
        {
            if (x == desk.X)
                MessageBox.Show("That's all Folks!");
            else
            {
                int one = mask & (1 << y + 1);
                int br = mask & (1 << y);
                if (br != 0)
                {
                    stack.Push(stack.Peek());
                    stackx.Push(stackx.Peek());
                    stacky.Push(stacky.Peek());
                    stackm.Push(stackm.Peek());
                    if (y + 1 < desk.Y)
                    {
                        mask -= 1 << y;
                        ++y;
                    }
                    else
                    {
                        mask -= 1 << y;
                        ++x;
                        y = 0;
                    }
                }
                else if (y == desk.Y - 2 && !(one != 0))
                {
                    stack.Push('V');
                    stackx.Push(x);
                    stacky.Push(y);
                    stackm.Push(mask);
                    ++x;
                    y = 0;
                }
                else if (y + 1 < desk.Y && !(one != 0))
                {
                    stack.Push('V');
                    stackx.Push(x);
                    stacky.Push(y);
                    stackm.Push(mask);
                    y += 2;
                }
                try
                {
                    PaintEventArgs pe = new PaintEventArgs(panel1.CreateGraphics(), new Rectangle(panel1.Location.X, panel1.Location.Y, panel1.Width, panel1.Height));
                    panel1_Paint(panel1, pe);
                }
                catch
                {
                    MessageBox.Show("That's all Folks!");
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (x == desk.X)
                MessageBox.Show("That's all Folks!");
            else
            {
                int br = mask & (1 << y);
                if (br != 0)
                {
                    stack.Push(stack.Peek());
                    stackx.Push(stackx.Peek());
                    stacky.Push(stacky.Peek());
                    stackm.Push(stackm.Peek());
                    if (y + 1 < desk.Y)
                    {
                        mask -= 1 << y;
                        ++y;
                    }
                    else
                    {
                        mask -= 1 << y;
                        ++x;
                        y = 0;
                    }
                }
                else if (x + 1 < desk.X)
                {
                    stack.Push('H');
                    stackx.Push(x);
                    stacky.Push(y);
                    stackm.Push(mask);
                    mask += (1 << y);
                    if (y + 1 < desk.Y)
                    {
                        ++y;
                    }
                    else
                    {
                        ++x;
                        y = 0;
                    }
                }
                try
                {
                    PaintEventArgs pe = new PaintEventArgs(panel1.CreateGraphics(), new Rectangle(panel1.Location.X, panel1.Location.Y, panel1.Width, panel1.Height));
                    panel1_Paint(panel1, pe);
                }
                catch 
                {
                    MessageBox.Show("That's all Folks!");
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {

                if (textBox1.Text != "" && textBox2.Text != "" && Convert.ToInt32(textBox1.Text) > 0 && Convert.ToInt32(textBox2.Text) > 0 && Convert.ToInt32(textBox1.Text) < 15 && Convert.ToInt32(textBox2.Text) < 8)
                {
                    desk = new Desk(Convert.ToInt32(textBox1.Text), Convert.ToInt32(textBox2.Text));
                    x = 0; y = 0; mask = 0;
                    stack.Clear();
                    stackx.Clear();
                    stacky.Clear();
                    stackm.Clear();
                    PaintEventArgs pe = new PaintEventArgs(panel1.CreateGraphics(), new Rectangle(panel1.Location.X, panel1.Location.Y, panel1.Width, panel1.Height));
                    Graphics g = pe.Graphics;
                    SolidBrush sb = new SolidBrush(Color.White);
                    g.FillRectangle(sb, 0, 0, panel1.Width, panel1.Height);
                    panel1_Paint(panel1, pe);
                }
                else
                {
                    MessageBox.Show("Oops, wrong input!");
                }
            }
            catch
            {
                MessageBox.Show("Oops, wrong input!");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (stack.Count == 0)
                return;
            PaintEventArgs pe = new PaintEventArgs(panel1.CreateGraphics(), new Rectangle(panel1.Location.X, panel1.Location.Y, panel1.Width, panel1.Height));
            Graphics g = pe.Graphics;
            SolidBrush sb = new SolidBrush(Color.White);
            g.FillRectangle(sb, 0, 0, panel1.Width, panel1.Height);
            char st = stack.Pop();
            int stx = stackx.Pop();
            int sty = stacky.Pop();
            int stm = stackm.Pop();
            try
            {
                while (st == stack.Peek() && stx == stackx.Peek() && sty == stacky.Peek() && stm == stackm.Peek())
                {
                    st = stack.Pop();
                    stx = stackx.Pop();
                    sty = stacky.Pop();
                    stm = stackm.Pop();
                }
            }
            catch
            {

            }
            mask = stm;
            x = stx;
            y = sty;
            panel1_Paint(panel1, pe);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Algo a = new Algo();
            a.Show();
        }

        public ProfileDP()
        {
            InitializeComponent();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            int x1 = -1, x2 = -1, y1 = -1, y2 = -1;
            try
            {
                int st = stack.Peek();
                if (st == 'V')
                {
                    x1 = stackx.Peek();
                    x2 = stackx.Peek();
                    y1 = stacky.Peek();
                    y2 = stacky.Peek() + 1;
                }
                if (st == 'H')
                {
                    x1 = stackx.Peek();
                    x2 = stackx.Peek() + 1;
                    y1 = stacky.Peek();
                    y2 = stacky.Peek();
                }
            }
            catch
            {
            }
            Graphics g = e.Graphics;
            desk.Draw(g);
            desk.DrawProfile(x, y, mask, g, x1, y1, x2, y2);
        }
    }
}
