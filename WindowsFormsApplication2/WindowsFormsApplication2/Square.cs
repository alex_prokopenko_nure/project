﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace WindowsFormsApplication2
{
    class Square
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double Side { get; set; }
        public Square(double x, double y, double side)
        {
            X = x; Y = y; Side = side;
        }
        public void Draw(Graphics g)
        {
            Pen pen = new Pen(Color.Black);
            pen.Width = 10;
            g.DrawRectangle(pen, (float)X, (float)Y, (float)Side, (float)Side);
        }
        public void Fill(Graphics g, Color col)
        {
            SolidBrush brush = new SolidBrush(col);
            g.FillRectangle(brush, (float)X + 5, (float)Y + 5, (float)Side - 10, (float)Side - 10);
        }
    }
}
